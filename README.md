# Interactive-Portfolio 

## A Resposive and attractive portfolio advanced | html and css only

A clean, beautiful and responsive portfolio templete with using only HTML and css.
Later on add some javascript for animation and designing.
visit the website [here.](https://aysi.me/)


## Sections:
* About me\
* Education\
* Cretification\
* Skills\
* Projects\
* Contact me

## Technology used:
- HTML
- CSS

## Illustration and Images
- [Undraw](https://undraw.co/)


## For the Future
I'm in thelove of css and i dont need to add javascript. So,i didn't add any of it 
If you can help me with these.Please don't hesitate to open a pull request.
- Add a page loader
- Add some Animation
- Enable Dark mode
- Add some more Section

## Reference 
Some Design idea are taken from open source revealers
and other well inerested geeks


##Note

i used SVGes to load the page faster and make lighter look.
